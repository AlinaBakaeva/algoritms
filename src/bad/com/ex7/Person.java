package bad.com.ex7;

public class Person {
    private String name;
    private int date;

    public Person(String name, int date) {
        this.name = name;
        this.date = date;
    }

    public int getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", date=" + date +
                '}';
    }
}

