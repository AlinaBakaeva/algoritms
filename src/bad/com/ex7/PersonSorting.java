package bad.com.ex7;

import java.util.ArrayList;

public class PersonSorting {
    public static void main(String[] args) {
        Person person1 = new Person("Мерзляков", 2001);
        Person person2 = new Person("Бакаева", 2002);
        Person person3 = new Person("Прокофьева", 2000);
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        sortingDate(persons);
        output(persons);
        sortingName(persons);
        output(persons);
    }

    private static ArrayList<Person> sortingName(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size() -1; i++) {
            for (int j = i + 1; j < persons.size(); j++) {
                int num = (persons.get(i).getName().compareTo(persons.get(j).getName()));
                if (num > 0) {
                    Person x = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, x);
                    continue;
                }
            }
        }
        return persons;
    }

    private static void output(ArrayList<Person> persons) {
        System.out.println(persons);
    }

    private static void sortingDate(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size(); i++) {
            for (int j = i + 1; j < persons.size(); j++) {
                if (persons.get(i).getDate() < persons.get(j).getDate()) {
                    Person temp = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, temp);

                }
            }

        }
    }

}
