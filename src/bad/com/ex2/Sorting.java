package bad.com.ex2;

import java.util.ArrayList;
import java.util.Arrays;

public class Sorting {
    public static void main(String[] args) {
        int[] numb = {6, 3, 2, 1, 5, 4, 7, 9, 8};
        for (int j=0; j < numb.length; j++){
            System.out.print(numb[j] + " ");
        }
        System.out.println();
        sorting(numb);
        for (int i = 0; i < numb.length; i++) {
            System.out.print(numb[i] + " ");
        }
    }

    private static void sorting(int[] numb) {
        for (int i = 0; i < 8; i++) {
            int min = numb[i];
            int minInd = i;
            for (int j = i + 1; j < 9; j++) {
                if (numb[j] < min) {
                    min = numb[j];
                    minInd = j;
                }
            }
            int x = numb[i];
            numb[i] = numb[minInd];
            numb[minInd] = x;
        }
    }
}

