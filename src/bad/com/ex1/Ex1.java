package bad.com.ex1;
import java.util.Arrays;
import java.util.Scanner;
public class Ex1 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = {1, 3, 5, 7, 9, 2, 4, 6, 8, 0};
        System.out.println("Введите число для поиска - ");
        int number = scanner.nextInt();
        int k = CheckingTheMassiv(array, number);

        System.out.println("Массив: ");
        MassivOutput(array);
        System.out.println();

        if (k >= 0){
            System.out.println("Элемент "+number+";"+" Индекс - "+k);
        }else{
            System.out.println("Элемент "+number+" не найден");
        }

    }
    private static void MassivOutput(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    private static int CheckingTheMassiv(int[] array, int number) {
        for (int index=0;index<array.length;index++){
            if (array[index] == number){
                return index;
            }
        }return -1;
    }
}