package bad.com.ex4;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ex4 {
    public static void main(String[] args) {
        int[] numb = {6, 3, 2, 1, 5, 4, 7, 9, 8};
        System.out.println(Arrays.toString(numb));
        ArrayList<Integer> numbers = new ArrayList(Arrays.asList(5, 3, 6, 1, 2, 4, 8, 9, 7));
        System.out.println(numbers);
        System.out.println();
        insertionSort(numb);
        System.out.println(Arrays.toString(numb));

        insertionSort(numbers);
        System.out.println(numbers);
    }

    private static void insertionSort(int[] numb) {
        for (int out = 1; out < numb.length; out++) {
            int temp = numb[out];
            int in = out;
            while (in > 0 && numb[in - 1] >= temp) {
                numb[in] = numb[in - 1];
                in--;
            }
            numb[in] = temp;
        }
    }

    private static void insertionSort(ArrayList<Integer> numbers) {
        for (int out = 1; out < numbers.size(); out++) {
            int temp = numbers.get(out);
            int in = out;
            while (in > 0 && numbers.get(in - 1) >= temp) {
                numbers.set(in, numbers.get(in - 1));
                in--;
            }
            numbers.set(in, temp);
        }


    }
}

