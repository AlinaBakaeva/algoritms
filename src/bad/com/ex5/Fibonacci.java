package bad.com.ex5;

import java.util.Scanner;

public class Fibonacci {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите n-ое число Фибоначчи: ");
        long n = scanner.nextInt();
        if (n < 0) {
            System.out.println("Введите целое число");
            return;
        }
        System.out.println(fibonacci(n));
        System.out.println(fibonacciRecursion(n));
    }

    private static int fibonacci(long n) {
        if (n == 0){
            return 0;
        }
        if (n == 1){
            return 1;
        }
        int F1 = 1;
        int F_2 = 1;
        int F_n = 1;
        for (int i = 3; i < n+1; i++) {
            F_n = F1 + F_2;
            F1 = F_2;
            F_2 = F_n;
        }
        return F_n;
    }

    private static int fibonacciRecursion(long n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
     return fibonacciRecursion(n-1)+ fibonacciRecursion(n-2);
    }
}
