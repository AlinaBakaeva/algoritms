package bad.com.ex5;

import java.math.BigInteger;
import java.util.Scanner;

import static java.math.BigInteger.ONE;

/**
 * Класс, для инициализации и вычисления факториала с помощью метода factorial
 * @author Bakaeva A.D
 */
public class Factorial {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите значения факториала: ");
        int numb = scanner.nextInt();
        if (numb < 0) {
            System.out.println("Введите целое число");
            return;
        }
        System.out.println(factorial(numb));
    }

    /**
     * Метод для вычисения факториала переменной
     * @param numb
     * @return
     */
    private static BigInteger factorial(int numb) {
        if (numb == 0 || numb == 1) {
            return ONE;
        }
        BigInteger m = ONE;
        for (int i = 1; i <= numb; i++) {
            m = m.multiply(BigInteger.valueOf(i));
        }
        return m;
    }
}
