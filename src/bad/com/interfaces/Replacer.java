package bad.com.interfaces;

import bad.com.interfaces.printers.IPrinter;
import bad.com.interfaces.readers.IReader;

class Replacer {
    private IReader reader;
    private IPrinter printer;

    Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace(){
        final String text = reader.read();
        final String replacedText = text.replaceAll(":\\)", ":-)");
        printer.print(replacedText);
    }
}