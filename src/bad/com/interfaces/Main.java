package bad.com.interfaces;

import bad.com.interfaces.printers.AdvConcolePrinter;
import bad.com.interfaces.printers.ConsolePrinters;
import bad.com.interfaces.printers.DecorativePrinter;
import bad.com.interfaces.printers.IPrinter;
import bad.com.interfaces.readers.IReader;
import bad.com.interfaces.readers.PredefinedReader;

public class Main {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader("Привет:) Пока-пока:)");
        IPrinter printer = new ConsolePrinters();
        IPrinter advPrinter = new AdvConcolePrinter();
        IPrinter decoPrinter = new DecorativePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, advPrinter);
        Replacer decoReplacer = new Replacer(reader, decoPrinter);
        replacer.replace();
        advReplacer.replace();
        decoReplacer.replace();
    }
}
