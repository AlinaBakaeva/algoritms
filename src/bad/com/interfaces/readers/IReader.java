package bad.com.interfaces.readers;

public interface IReader {
    String read();
}
