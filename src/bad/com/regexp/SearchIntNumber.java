package bad.com.regexp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchIntNumber {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile( "(пре|при|При|Пре)[а-яA-яЁё]+" );

        Matcher matcher = pattern.matcher( "" );
        try (BufferedReader bufferedReader = new BufferedReader( new FileReader( "src/bad/com/regexp/input.txt" ) )) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset( s );
                while (matcher.find()) {
                    System.out.println( matcher.group() );
                }
            }
        }
    }
}