package bad.com.ex3;

import java.util.ArrayList;
import java.util.Arrays;

public class BubbleSortingArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList(Arrays.asList(5, 3, 6, 1, 2, 4, 8, 9, 7));
        System.out.println(numbers);
        System.out.println();
        sortingBubble(numbers);
        System.out.println(numbers);
    }

    private static void sortingBubble(ArrayList<Integer> numbers) {
        boolean sorting = true;
        while (sorting) {
            sorting = false;
            for (int j = 0; j < numbers.size() - 1; j++) {
                if (numbers.get(j) > numbers.get(j + 1)) {
                    int temp = numbers.get(j);
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, temp);
                    sorting = true;
                }
            }
        }
    }
}