package bad.com.ex3;

public class BubbleSorting {
    public static void main(String[] args) {
        int[] numb = {6, 3, 2, 1, 5, 4, 7, 9, 8};
        for (int value : numb) {
            System.out.print(value + " ");
        }
        System.out.println();
        sortingBubble(numb);
        for (int value : numb) {
            System.out.print(value + " ");
        }
    }

    private static void sortingBubble(int[] numb) {
        boolean sorted = true;
        while (sorted) {
            sorted = false;
            for (int i = 0; i < numb.length - 1; i++) {
                if (numb[i] > numb[i + 1]) {
                    int temp = numb[i];
                    numb[i] = numb[i + 1];
                    numb[i + 1] = temp;
                    sorted = true;

                }
            }
        }

    }
}

