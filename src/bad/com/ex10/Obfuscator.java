package bad.com.ex10;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Obfuscator {
    public static void main(String[] args) throws IOException {
        String listLine = fileReader();
        //путь присваиваем строке
        String strings = "C:\\Work\\Algorithms\\src\\bad\\com\\binarySearch\\BinarySearch.java";


        String nameFile = fileName(strings);
        String path = pathNewFile(strings);

    }

    /**
     * метод считывает с файла код и переделывает его в мультистроку
     *
     * @return возращает мультистроку
     */
    private static String fileReader() {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get("C:\\Work\\Algorithms\\src\\bad\\com\\binarySearch\\BinarySearch.java"));

            for (String s : list) {
                listLine.append(s); //listLine += s
            }
        } catch (IOException e) {
            System.out.println("not found");
        }
        return listLine.toString();
    }

    /**
     * метод изменяет имя классу и добавляет приставку New
     *
     * @param strings путь файла строкового типа
     * @return возращает новое имя класса
     */
    public static String newNameСlass(String strings, String nameFile) {
        return strings.replaceAll(nameFile, "New" + nameFile);
    }

    /**
     * метод ищет имя файла
     *
     * @param path путь к файлу
     * @return возращает имя файла
     */
    public static String fileName(String path) {
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(path.split("/")));
        ArrayList<String> fileNames = new ArrayList<>(Arrays.asList(strings.get(strings.size() - 1).split("\\.")));
        String nameFile = fileNames.get(0);
        return nameFile;
    }

    /**
     * метод создает путь к новому файлу
     *
     * @param path путь к файлу
     * @return возращает новый путь к новому файлу
     */
    public static String pathNewFile(String path) {
        String pathNewFile = "";
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(path.split("/")));
        for (int i = 0; i < strings.size() - 1; i++) {
            pathNewFile = pathNewFile + strings.get(i) + "/";
        }
        return pathNewFile;
    }

    /**
     * метод удаляет коментарии
     *
     * @param listLine текст кода в виде мультистроки
     * @return возращает мультистроку без коментариев
     */
    public static String deleteComment(String listLine) {

        return listLine.replaceAll("(/\\*.+?\\*/)|(//.+)", "");
    }

    /**
     * метод удаляет символы перехода на новую строку
     *
     * @param listLine текст кода в виде мультистроки
     * @return возращает мультистроку без символов перехода на новую строку
     */
    public static String deleteSymbolTransitionNewLine(String listLine) {

        return listLine.replaceAll("\n", "");
    }

    /**
     * метод удаляе лишние пробелы(оставляет только 1 пробел)
     *
     * @param listLine текст кода в виде мультистроки
     * @return возращает мультистроку без лишних пробелов
     */
    public static String deleteGaps(String listLine) {

        return listLine.replaceAll("\\s+(?![^\\d\\s])", " ");
    }
}

