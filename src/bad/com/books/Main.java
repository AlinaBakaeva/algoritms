package bad.com.books;

import java.util.ArrayList;
import java.util.Scanner;

public class Main { public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    Book book1 = new Book("Грокаем алгоритмы", "Бхаргава", Genre.EDUCATIONAL, "C:/books/GrokaemAlgorithms", 2017);
    Book book2 = new Book("451° по Фаренгейту", "Бредбери", Genre.SCIENCE_FICTION, "C:/books/451°Fahrenheit", 1953);
    Book book3 = new Book("1984", "Оруэл", Genre.SCIENCE_FICTION, "C:/books/1984", 1949);
    Book book4 = new Book("Мастер и Маргаритта", "Булгаков", Genre.PROSE, "C:/books/TheMasterAndMargarita", 1966);
    Book book5 = new Book("Маленький принц", "Сент-Экзюпери", Genre.FAIRY, "C:/books/ALittlePrince", 1958);
    Book book6 = new Book("Вино из одуванчиков", "Бредбери", Genre.SCIENCE_FICTION, "C:/books/WineFromDandelions", 1957);
    Book book7 = new Book("Алые паруса", "Грин", Genre.PROSE, "C:/books/ScarletSails", 1923);
    Book book8 = new Book("Гарри Поттер и Дары Смерти", "Роулинг", Genre.FANTASY, "C:/books/HarryPotterAndTheDeathlyHallows", 2007);
    Book book9 = new Book("Шантарам", "Робертс", Genre.ADVENTURE, "C:/books/Shantaram", 2003);
    Book book10 = new Book("Гензель и Гретель", "Бхаргава", Genre.FAIRY, "C:/books/HanselAndGretel", 1812);

    Book[] books = {book1, book2, book3, book4, book5, book6, book7, book8, book9, book10};

    out(books);
    sorting(books);
    System.out.println("\nСортировка по фамилиям авторов:");
    out(books);
    sortingOfPublishYear(books);
    System.out.println("\nСоритровка по году издания:");
    out(books);

    System.out.print("\nВведите название книги: ");
    String title = scanner.nextLine();
    outputFindTitle(books, title);

    System.out.print("\nВведите жанр книг: ");
    String genre = scanner.nextLine();
    outputFindGenre(books, genre);

    System.out.print("\nВведите фамилию автора: ");
    String author = scanner.nextLine();
    outputFindAuthor(books, author);
}

    /**
     * Сортирует объекты массива по фамилиям авторов по алфавиту
     *
     * @param books массив объектов
     */
    private static void sorting(Book[] books) {
        for (int i = 0; i < books.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < books.length; j++) {
                int isCompare = books[min].getAuthor().compareTo(books[j].getAuthor());
                if (isCompare > 0) {
                    min = j;
                }
            }
            if (i != min) {
                Book temp = books[i];
                books[i] = books[min];
                books[min] = temp;
            }
        }
    }

    /**
     * Сортирует массив объектов по году публикации по убыванию
     *
     * @param books массив объектов
     */
    private static void sortingOfPublishYear(Book[] books) {
        for (int i = 0; i < books.length - 1; i++) {
            int max = books[i].getPublishYear();
            int maxI = i;
            for (int j = i + 1; j < books.length; j++) {
                if (max < books[j].getPublishYear()) {
                    max = books[j].getPublishYear();
                    maxI = j;
                }
            }
            Book temp = books[i];
            books[i] = books[maxI];
            books[maxI] = temp;
        }
    }


    /**
     * Ищет книгу(и) с введенным с консоли названием
     *
     * @param books массив объектов
     * @param title введенное с консоли название
     * @return массив объектов с данным названием
     */
    private static ArrayList<Book> find(Book[] books, String title) {
        ArrayList<Book> booksTitle = new ArrayList<>();
        for (Book book : books) {
            if (title.equalsIgnoreCase(book.getTitle())) {
                booksTitle.add(book);
            }
        }
        return booksTitle;
    }

    /**
     * Находит книгу по жанру
     *
     * @param books массив объектов
     * @param genre введенный с консоли жанр
     * @return массив с заданным жанром
     */
    private static ArrayList<Book> findGenre(Book[] books, String genre) {
        ArrayList<Book> findGenre = new ArrayList<>();

        Genre bookGenre = Genre.FAIRY;

        if (genre.equalsIgnoreCase("Фантастика")) {
            bookGenre = Genre.FANTASY;
        }
        if (genre.equalsIgnoreCase("Научная фантастика")) {
            bookGenre = Genre.SCIENCE_FICTION;
        }
        if (genre.equalsIgnoreCase("Приключения")) {
            bookGenre = Genre.ADVENTURE;
        }
        if (genre.equalsIgnoreCase("Учебная")) {
            bookGenre = Genre.EDUCATIONAL;
        }
        if (genre.equalsIgnoreCase("Проза")) {
            bookGenre = Genre.PROSE;
        }

        for (Book book : books) {
            if (bookGenre.equals(book.getGenre())) {
                findGenre.add(book);
            }
        }
        return findGenre;
    }

    /**
     * Находит книгу(и) с введенной фамилией
     *
     * @param books  массив объектов
     * @param author введенная с консоли фамилия автора
     * @return массив с фамилиями
     */
    private static ArrayList<Book> findAuthor(Book[] books, String author) {
        ArrayList<Book> findAuthor = new ArrayList<>();

        for (Book book : books) {
            if (author.equalsIgnoreCase(book.getAuthor())) {
                findAuthor.add(book);
            }
        }
        return findAuthor;
    }

    /**
     * Выводит найденные книги
     *
     * @param books массив объектов
     * @param title введенное с консоли название
     */
    private static void outputFindTitle(Book[] books, String title) {
        ArrayList<Book> booksTitle = find(books, title);
        if (booksTitle.isEmpty()) {
            System.out.println("Книг с таким названием нет");
        } else {
            for (Book book : booksTitle) {
                System.out.println(book + " ");
            }
        }
    }

    /**
     * Выводит найденные книги
     *
     * @param books массив объектов
     * @param genre введенный с консоли жанр
     */
    private static void outputFindGenre(Book[] books, String genre) {
        ArrayList<Book> booksGenre = findGenre(books, genre);
        if (booksGenre.isEmpty()) {
            System.out.println("Книг с таким жанром нет");
        } else {
            for (Book book : booksGenre) {
                System.out.println(book + " ");
            }
        }
    }

    /**
     * Выводит найденные книги
     *
     * @param books  массив объектов
     * @param author введенное с консоли название
     */
    private static void outputFindAuthor(Book[] books, String author) {
        ArrayList<Book> booksAuthor = findAuthor(books, author);
        if (booksAuthor.isEmpty()) {
            System.out.println("Книг с таким автором нет");
        } else {
            for (Book book : booksAuthor) {
                System.out.println(book + " ");
            }
        }
    }

    /**
     * Выводит массив объектов
     *
     * @param books массив объектов
     */
    private static void out(Book[] books) {
        for (Book book : books) {
            System.out.println(book + " ");
        }
    }
}

