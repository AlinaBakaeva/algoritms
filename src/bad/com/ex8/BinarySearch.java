package bad.com.ex8;

public class BinarySearch {
    public static void main(String[] args) {
        int arr[] = {-8, 3, 10, 14, 16, 25};
        System.out.println( binarySearch( arr, 6 ) );
        System.out.println( binarySearch( arr, 7 ) );
    }

    private static int binarySearch(int[] arr, int num) {
        int low = 0;
        int high = arr.length - 1;
        while (low <= high) {
            int mid = (high + low) / 2;
            if (arr[mid] == num) {
                return mid;
            }
            if (num < arr[mid]) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }
}

