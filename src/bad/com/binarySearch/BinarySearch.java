package bad.com.binarySearch;
import java.util.Scanner;
public class BinarySearch {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int[] numb ={-2,0,1,3,5,6,10,12};
        System.out.println("Введите число для поиска: ");
        int x = scanner.nextInt();
        System.out.println("Элемент "+x+" находится на позиции - "+binarySearch(numb,x));
    }

    private static int binarySearch(int[] numb, int x) {
        int first = 0;
        int last = numb.length;
        int mid = 0;
        while (first<=last){
            mid = (last + first)/2;
            if (x < numb[mid]){
                last = mid;
            }
            if (x> numb[mid]){
                first = mid;
            }
            if (x==numb[mid]){
                break;
            }
        }
        return mid+1;
    }
}
