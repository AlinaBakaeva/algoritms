package bad.com.petterns.iceCream.decorators;

import bad.com.petterns.iceCream.objects.Component;

public class Syrup extends Decorator {
    public Syrup(Component component) {
        super(component);
    }
    @Override
    void afterCooking() {
        System.out.println("Добавлен сироп");
    }
}
