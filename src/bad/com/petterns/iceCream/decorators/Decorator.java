package bad.com.petterns.iceCream.decorators;

import bad.com.petterns.iceCream.objects.Component;

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    abstract void afterCooking();

    @Override
    public void cooking() {
        component.cooking();
        afterCooking();
    }
}


