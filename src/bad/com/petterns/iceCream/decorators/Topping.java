package bad.com.petterns.iceCream.decorators;

import bad.com.petterns.iceCream.objects.Component;

public class Topping extends Decorator {
    public Topping(Component component) {
        super(component);
    }

    @Override
    void afterCooking() {
        System.out.println("Добавлен сироп");
    }
}
