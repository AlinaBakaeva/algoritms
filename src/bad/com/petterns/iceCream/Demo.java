package bad.com.petterns.iceCream;

import bad.com.petterns.iceCream.decorators.Syrup;
import bad.com.petterns.iceCream.decorators.Topping;
import bad.com.petterns.iceCream.objects.Component;
import bad.com.petterns.iceCream.objects.IceCream;
import bad.com.petterns.iceCream.objects.Smoothie;
import bad.com.petterns.iceCream.objects.Yogurt;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Component iceCream;
        Component smoothie;
        Component yogurt;
        Random random = new Random();

        boolean addSyrup = random.nextBoolean();
        if (addSyrup) {
            iceCream = new Syrup(new IceCream());
            smoothie = new Syrup(new Smoothie());
            yogurt = new Syrup(new Yogurt());
        } else {
            iceCream = new IceCream();
            smoothie = new Smoothie();
            yogurt = new Yogurt();
        }

        iceCream.cooking();
        smoothie.cooking();
        yogurt.cooking();

        System.out.println();

        boolean addTopping = random.nextBoolean();
        if (addTopping) {
            iceCream = new Topping(new IceCream());
            smoothie = new Topping(new Smoothie());
            yogurt = new Topping(new Yogurt());
        } else {
            iceCream = new IceCream();
            smoothie = new Smoothie();
            yogurt = new Yogurt();
        }

        iceCream.cooking();
        smoothie.cooking();
        yogurt.cooking();

        System.out.println();

        iceCream = new Topping(new Syrup(new IceCream()));
        iceCream.cooking();

        smoothie = new Topping(new Syrup(new Smoothie()));
        smoothie.cooking();

        yogurt = new Topping(new Syrup(new Yogurt()));
        yogurt.cooking();
    }
}


